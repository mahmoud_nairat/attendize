@extends('Shared.Layouts.Master')

@section('title')
    @parent
    @lang("Organiser.organizers")
@endsection

@section('top_nav')
    @include('ManageSuperuser.Partials.TopNav')
@stop
@section('page_title')
    @lang("Organiser.organizers", ["name"=>$organiser->name])
@stop

@section('menu')
    @include('ManageSuperuser.Partials.Sidebar')
@stop

@section('head')

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" integrity="sha256-szHusaozbQctTn4FX+3l5E0A5zoxz7+ne4fr8NgWJlw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js" integrity="sha256-Gk+dzc4kV2rqAZMkyy3gcfW6Xd66BhGYjVWa/FjPu+s=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js" integrity="sha256-0rg2VtfJo3VUij/UY9X0HJP7NET6tgAY98aMOfwP0P8=" crossorigin="anonymous"></script>

    {!! HTML::script('https://maps.googleapis.com/maps/api/js?libraries=places&key='.env("GOOGLE_MAPS_GEOCODING_KEY")) !!}
    {!! HTML::script('vendor/geocomplete/jquery.geocomplete.min.js')!!}
    {!! HTML::script('vendor/moment/moment.js')!!}
    {!! HTML::script('vendor/fullcalendar/dist/fullcalendar.min.js')!!}
    <?php
    if(Lang::locale()!="en")
        echo HTML::script('vendor/fullcalendar/dist/lang/'.Lang::locale().'.js');
    ?>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#example').DataTable();
        });
    </script>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Phone</th>
                        <th>Active</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($organisers as $organiser)
                        <tr>
                            <td>{{ $organiser->name }}</td>
                            <td>{{ $organiser->email }}</td>
                            <td>{{ $organiser->phone }}</td>
                            <td>
                                <div class="form-group">
                                    @if($organiser->account->is_active == 1)
                                        <div data-id="{{ $organiser->id }}"
                                           data-route="{{ route('postOrganiserToggleStatus', ['organiser_id' => $organiser->id]) }}"
                                           data-type="organiser" data-value="1" href="javascript:void(0);"
                                           class="toggle-status btn btn-success">
                                            <i class="ico-check"></i> @lang("basic.active")
                                        </div>
                                    @else
                                        <div data-id="{{ $organiser->id }}"
                                           data-route="{{ route('postOrganiserToggleStatus', ['organiser_id' => $organiser->id]) }}"
                                           data-type="organiser" data-value="0" href="javascript:void(0);"
                                           class="toggle-status btn btn-danger">
                                            <i class="ico-cancel"></i> @lang("basic.disactive")
                                        </div>
                                    @endif
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Name</th>
                        <th>E-Mail</th>
                        <th>Phone</th>
                        <th>Active</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@stop
