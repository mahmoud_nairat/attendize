@extends('Shared.Layouts.Master')

@section('title')
    @parent
    @lang("Organiser.dashboard")
@endsection

@section('top_nav')
    @include('ManageSuperuser.Partials.TopNav')
@stop
@section('page_title')
    @lang("Organiser.organiser_name_dashboard", ["name"=>$organiser->name])
@stop

@section('menu')
    @include('ManageSuperuser.Partials.Sidebar')
@stop

@section('head')

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css" integrity="sha256-szHusaozbQctTn4FX+3l5E0A5zoxz7+ne4fr8NgWJlw=" crossorigin="anonymous" />
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.4/raphael-min.js" integrity="sha256-Gk+dzc4kV2rqAZMkyy3gcfW6Xd66BhGYjVWa/FjPu+s=" crossorigin="anonymous"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js" integrity="sha256-0rg2VtfJo3VUij/UY9X0HJP7NET6tgAY98aMOfwP0P8=" crossorigin="anonymous"></script>

    {!! HTML::script('https://maps.googleapis.com/maps/api/js?libraries=places&key='.env("GOOGLE_MAPS_GEOCODING_KEY")) !!}
    {!! HTML::script('vendor/geocomplete/jquery.geocomplete.min.js')!!}
    {!! HTML::script('vendor/moment/moment.js')!!}
    {!! HTML::script('vendor/fullcalendar/dist/fullcalendar.min.js')!!}
    <?php
    if(Lang::locale()!="en")
        echo HTML::script('vendor/fullcalendar/dist/lang/'.Lang::locale().'.js');
    ?>
    {!! HTML::style('vendor/fullcalendar/dist/fullcalendar.css')!!}

    <script>
        $(function() {
            $('#calendar').fullCalendar({
                locale: '{{ Lang::locale() }}',
                events: {!! $calendar_events !!},
                header: {
                    left:   'prev,',
                    center: 'title',
                    right:  'next'
                },
                dayClick: function(date, jsEvent, view) {

                }
            });
        });
    </script>

    <style>
        svg {
            width: 100% !important;
        }
    </style>
@stop

@section('content')
    <div class="row">
        <div class="col-sm-6">
            <div class="stat-box">
                <h3>
                    {{count(json_decode($calendar_events))}}
                </h3>
            <span>
                @lang("Organiser.events")
            </span>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="stat-box">
                <h3>
                    {{$organiser->attendees->count()}}
                </h3>
            <span>
                @lang("Organiser.tickets_sold")
            </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading panel-default">
                    <h3 class="panel-title">
                        @lang("Dashboard.tickets_sold")
                <span style="color: green; float: right;">
                    {{$organiser->attendees->count()}}

                </span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="chart-wrap">
                        <div style="height:200px;" class="statChart" id="theChart"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading panel-default">
                    <h3 class="panel-title">
                        @lang("Dashboard.ticket_sales_volume")
                        <span style="color: green; float: right;">

                            @lang("basic.total")
                        </span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="chart-wrap">
                        <div style="height: 200px;" class="statChart" id="theChart3"></div>
                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading panel-default">
                    <h3 class="panel-title">
                        @lang("Dashboard.event_page_visits")
                        <span style="color: green; float: right;">

                        </span>
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="chart-wrap">
                        <div style="height: 200px;" class="statChart" id="theChart2"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel">
                <div class="panel-heading panel-default">
                    <h3 class="panel-title">
                        @lang("Dashboard.registrations_by_ticket")
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="chart-wrap">
                        <div style="height:200px;" class="statChart" id="pieChart"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-8">

            <h4 style="margin-bottom: 25px;margin-top: 20px;">@lang("Organiser.event_calendar")</h4>
            <div id="calendar"></div>


            <h4 style="margin-bottom: 25px;margin-top: 20px;">@lang("Public_ViewOrganiser.upcoming_events")</h4>
            @if($upcoming_events->count())
                @foreach($upcoming_events as $event)
                    @include('ManageSuperuser.Partials.EventPanel')
                @endforeach
            @else
                <div class="alert alert-success alert-lg">
                    @lang("Organiser.no_upcoming_events") <a href="#"
                                                     data-href="{{route('showCreateEvent', ['organiser_id' => $organiser->id])}}"
                                                     class=" loadModal">@lang("Organiser.no_upcoming_events_click")</a>
                </div>
            @endif
        </div>
        <div class="col-md-4">
            <h4 style="margin-bottom: 25px;margin-top: 20px;">@lang("Order.recent_orders")</h4>
            @if($organiser->orders->count())
                <ul class="list-group">
                    @foreach($organiser->orders()->orderBy('created_at', 'desc')->take(5)->get() as $order)
                        <li class="list-group-item">
                            <h6 class="ellipsis">
                                <a href="{{ route('showEventDashboard', ['event_id' => $order->event->id]) }}">
                                    {{ $order->event->title }}
                                </a>
                            </h6>
                            <p class="list-group-text">
                                <a href="{{ route('showEventOrders', ['event_id' => $order->event_id, 'q' => $order->order_reference]) }}">
                                    <b>#{{ $order->order_reference }}</b></a> -
                                <a href="{{ route('showEventAttendees', ['event_id'=>$order->event->id,'q'=>$order->order_reference]) }}">
                                    <strong>{{ $order->full_name }}</strong>
                                </a> {{ @trans("Order.registered") }}
                                    {{ $order->attendees()->withTrashed()->count() }} {{ @trans("Order.tickets") }}
                            </p>
                            <h6>
                                {{ $order->created_at->diffForHumans() }} &bull; <span
                                        style="color: green;">{{ $order->event->currency_symbol }}{{ $order->amount }}</span>
                            </h6>
                        </li>
                    @endforeach
                    @else
                        <div class="alert alert-success alert-lg">
                            @lang("Order.no_recent_orders")
                        </div>
                    @endif
                </ul>

        </div>
    </div>
 
    <script>

        var chartData = {!! $chartData  !!};
        var ticketData = {!! $ticketData  !!};

        new Morris.Donut({
            element: 'pieChart',
            data: ticketData,
        });

        new Morris.Line({
            element: 'theChart3',
            data: chartData,
            xkey: 'date',
            ykeys: ['sales_volume'],
            labels: ["@lang("Dashboard.sales_volume")"],
            xLabels: 'day',
            xLabelAngle: 30,
            yLabelFormat: function (x) {
                // add something here
            },
            xLabelFormat: function (x) {
                return formatDate(x);
            }
        });
        new Morris.Line({
            element: 'theChart2',
            data: chartData,
            xkey: 'date',
            //ykeys: ['views', 'unique_views'],
            //labels: ['Event Page Views', 'Unique views'],
            ykeys: ['views'],
            labels: ["@lang("Dashboard.event_views")"],
            xLabels: 'day',
            xLabelAngle: 30,
            xLabelFormat: function (x) {
                return formatDate(x);
            }
        });
        new Morris.Line({
            element: 'theChart',
            data: chartData,
            xkey: 'date',
            ykeys: ['tickets_sold'],
            labels: ["@lang("Dashboard.tickets_sold")"],
            xLabels: 'day',
            xLabelAngle: 30,
            lineColors: ['#0390b5', '#0066ff'],
            xLabelFormat: function (x) {
                return formatDate(x);
            }
        });
        function formatDate(x) {
            var m_names = <?=json_encode(array_filter(explode("|", trans("basic.months_short")))); ?>;
            var sup = "";
            var curr_date = x.getDate();

            <?php if(Lang::locale()=="en") { ?>
            if (curr_date == 1 || curr_date == 21 || curr_date == 31) {
                sup = "st";
            }
            else if (curr_date == 2 || curr_date == 22) {
                sup = "nd";
            }
            else if (curr_date == 3 || curr_date == 23) {
                sup = "rd";
            }
            else {
                sup = "th";
            }
            <?php } ?>

            return curr_date + sup + ' ' + m_names[x.getMonth() + 1];
        }



    </script>
@stop
