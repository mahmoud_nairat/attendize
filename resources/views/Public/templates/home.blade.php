<!doctype html>
<html lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="author" content="DSAThemes">
    <meta name="description" content="PowerNode - Multi-Purpose Landing Page with Page Builder"/>
    <meta name="keywords" content="Responsive, HTML5 template, DSAThemes, Multi-Purpose, Startup, One Page, Landing, Business, Creative, Corporate, Agency Template, Project, Mobile App">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- SITE TITLE -->
    <title>Eventati</title>

    <!-- FAVICON AND TOUCH ICONS  -->
    <link rel="shortcut icon" href="{{asset('assets/front_end')}}/{{asset('assets/front_end')}}/images/favicon.ico" type="image/x-icon">
    <link rel="icon" href="{{asset('assets/front_end')}}/{{asset('assets/front_end')}}/images/favicon.ico" type="image/x-icon">
    <link rel="apple-touch-icon" sizes="144x144" href="{{asset('assets/front_end')}}/{{asset('assets/front_end')}}/images/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{{asset('assets/front_end')}}/{{asset('assets/front_end')}}/images/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/front_end')}}/{{asset('assets/front_end')}}/images/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" href="{{asset('assets/front_end')}}/{{asset('assets/front_end')}}/images/icons/apple-touch-icon.png">

    <!-- BOOTSTRAP CSS -->
    <link href="{{asset('assets/front_end')}}/css/bootstrap.min.css" rel="stylesheet">

    <!-- FONT ICONS -->
    <link href="{{asset('assets/front_end')}}/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{asset('assets/front_end')}}/css/themify-icons.css" rel="stylesheet">

    <!-- GOOGLE FONTS -->
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,900,700,500,300' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet">

    <!-- PLUGINS STYLESHEET -->
    <link href="{{asset('assets/front_end')}}/css/owl.carousel.css" rel="stylesheet">
    <link href="{{asset('assets/front_end')}}/css/flexslider.css" rel="stylesheet">
    <link href="{{asset('assets/front_end')}}/css/magnific-popup.css" rel="stylesheet">

    <!-- On Scroll Animations -->
    <link href="{{asset('assets/front_end')}}/css/animate.min.css" rel="stylesheet">

    <!-- TEMPLATE CSS -->
    <link href="{{asset('assets/front_end')}}/css/style.css" rel="stylesheet">

    <!-- RESPONSIVE CSS -->
    <link href="{{asset('assets/front_end')}}/css/responsive.css" rel="stylesheet">
</head>
<body>
<!-- PRELOADER
    ============================================= -->
<div id="loader-wrapper">
    <div id="loader"></div>
</div>



<!-- PAGE CONTENT
============================================= -->
<div id="page" class="page">



    <!-- HEADER-2
    ============================================= -->
    <header id="header-2" class="header">

        <!-- navbar default White Background: -->
        <!--
            <nav class="navbar">
        -->

        <!-- navbar fixed on top White Background: -->
        <!--
            <nav class="navbar navbar-fixed-top bg-white">
        -->

        <!-- navbar fixed on top Transparent Background : -->
        <nav class="navbar navbar-fixed-top no-bg">

            <div class="container">

                <!-- Navigation Bar -->
                <div class="navbar-header">

                    <!-- Responsive Menu Button -->
                    <button type="button" id="nav-toggle" class="navbar-toggle text-right" data-toggle="collapse" data-target="#navigation-menu">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- LOGO IMAGE -->
                    <!-- Recommended sizes 120x16px; -->
                    <a class="navbar-brand logo-white" style="padding: 5px;" href="#intro-4"><img style="max-width: 170px;" src="{{asset('/assets/')}}/images/logo-light.png" alt="logo"></a>
                    <a class="navbar-brand logo-black" style="padding: 5px;" href="#intro-4"><img style="max-width: 170px;" src="{{asset('/assets/')}}/images/logo-dark.png" alt="logo"></a>

                </div>	<!-- End Navigation Bar -->

                <!-- Navigation Menu -->
                <div id="navigation-menu" class="collapse navbar-collapse editContent">
                    <ul class="nav navbar-nav navbar-right">

                        <li class="nav-link"><a href="#services-1-2">Services</a></li>
                        <li class="nav-link"><a href="#portfolio-1-2">Events</a></li>
                        <li class="nav-link"><a href="#pricing-3-1">Pricing</a></li>
                        <li class="nav-link"><a class="header-btn" href="#contacts-1-3">Request a Demo</a></li>

                    </ul>
                </div>  <!-- End Navigation Menu -->

            </div>	  <!-- End container -->

        </nav>	   <!-- End navbar  -->

    </header>	<!-- END HEADER-2 -->



    <!-- INTRO-4
    ============================================= -->
    <section id="intro-4" class="intro-section division">
        <div class="container">
            <div id="intro-4-content" class="row intro-row-180">


                <!-- INTRO TEXT -->
                <div class="col-lg-10 col-lg-offset-1 intro-txt text-center white-color editContent">

                    <!-- Title -->
                    <h2 class="intro-medium">We have everything to make your business a success</h2>
                </div>


            </div>	 <!-- End Intro Content -->
        </div>    <!-- End container -->
    </section>	<!-- END INTRO-4 -->



    <!-- SERVICES-1-2
    ============================================= -->
    <section id="services-1-2" class="wide-50 services-section division bg-edit">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title editContent">
                    <h3>Powerful Experience</h3>
                    <p>Aliquam a augue suscipit, bibendum luctus neque vestibulum laoreet rhoncus ipsum, bibendum
                        tempor vulputate varius
                    </p>
                </div>
            </div>


            <div class="row">
                <ul>

                    <!-- SERVICE BOX #1 -->
                    <li class="col-sm-6 col-md-4 sbox-1 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="200">

                        <!-- Icon -->
                        <div class="sbox-1-icon theme-color"><span class="ti-desktop"></span></div>

                        <!-- Text -->
                        <div class="sbox-1-txt">
                            <h4>Responsive Layout</h4>
                            <p>Praesent semper, lacus cursus porta, feugiat primis in pharetra ultrice ligula and posuere potenti enim
                                magna felis ut lorem egestas mauris
                            </p>
                        </div>

                    </li>


                    <!-- SERVICE BOX #2 -->
                    <li class="col-sm-6 col-md-4 sbox-1 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="300">

                        <!-- Icon -->
                        <div class="sbox-1-icon theme-color"><span class="ti-vector"></span></div>

                        <!-- Text -->
                        <div class="sbox-1-txt">
                            <h4>Modern Design</h4>
                            <p>Praesent semper, lacus cursus porta, feugiat primis in pharetra ultrice ligula and posuere potenti enim
                                magna felis ut lorem egestas mauris
                            </p>
                        </div>

                    </li>


                    <!-- SERVICE BOX #3 -->
                    <li class="col-sm-6 col-md-4 sbox-1 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="400">

                        <!-- Icon -->
                        <div class="sbox-1-icon theme-color"><span class="ti-panel"></span></div>

                        <!-- Text -->
                        <div class="sbox-1-txt">
                            <h4>Extremely Flexible</h4>
                            <p>Praesent semper, lacus cursus porta, feugiat primis in pharetra ultrice ligula and posuere potenti enim
                                magna felis ut lorem egestas mauris
                            </p>
                        </div>

                    </li>


                    <!-- SERVICE BOX #4 -->
                    <li class="col-sm-6 col-md-4 sbox-1 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="500">

                        <!-- Icon -->
                        <div class="sbox-1-icon theme-color"><span class="ti-shortcode"></span></div>

                        <!-- Text -->
                        <div class="sbox-1-txt">
                            <h4>Organized Code</h4>
                            <p>Praesent semper, lacus cursus porta, feugiat primis in pharetra ultrice ligula and posuere potenti enim
                                magna felis ut lorem egestas mauris
                            </p>
                        </div>

                    </li>


                    <!-- SERVICE BOX #5 -->
                    <li class="col-sm-6 col-md-4 sbox-1 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="600">

                        <!-- Icon -->
                        <div class="sbox-1-icon theme-color"><span class="ti-image"></span></div>

                        <!-- Text -->
                        <div class="sbox-1-txt">
                            <h4>Parallax Backgrounds</h4>
                            <p>Praesent semper, lacus cursus porta, feugiat primis in pharetra ultrice ligula and posuere potenti enim
                                magna felis ut lorem egestas mauris
                            </p>
                        </div>

                    </li>


                    <!-- SERVICE BOX #6 -->
                    <li class="col-sm-6 col-md-4 sbox-1 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="700">

                        <!-- Icon -->
                        <div class="sbox-1-icon theme-color"><span class="ti-reload"></span></div>

                        <!-- Text -->
                        <div class="sbox-1-txt">
                            <h4>Free Updates</h4>
                            <p>Praesent semper, lacus cursus porta, feugiat primis in pharetra ultrice ligula and posuere potenti enim
                                magna felis ut lorem egestas mauris
                            </p>
                        </div>

                    </li>

                </ul>
            </div>     <!-- End row -->
        </div>	    <!-- End container -->
    </section>	 <!-- END SERVICES-1-2 -->

    <!-- STATISTIC-1-2
    ============================================= -->
    <div id="statistic-1-2" class="bg-scroll p-top-80 p-bottom-30 statistic-banner division">
        <div class="container">
            <div class="row">


                <!-- STATISTIC BLOCK #1 -->
                <div class="col-xs-3 statistic-block text-center m-bottom-50 editContent animated" data-animation="bounceIn" data-animation-delay="200">
                    <span class="ti-shortcode theme-color"></span>
                    <div class="statistic-number">2468</div>
                    <p>Lines of Code</p>
                </div>


                <!-- STATISTIC BLOCK #2 -->
                <div class="col-xs-3 statistic-block text-center m-bottom-50 editContent animated" data-animation="bounceIn" data-animation-delay="400">
                    <span class="ti-comments-smiley theme-color"></span>
                    <div class="statistic-number">443</div>
                    <p>Satisfied clients</p>
                </div>


                <!-- STATISTIC BLOCK #3 -->
                <div class="col-xs-3 statistic-block text-center m-bottom-50 editContent animated" data-animation="bounceIn" data-animation-delay="600">
                    <span class="ti-twitter theme-color"></span>
                    <div class="statistic-number">845</div>
                    <p>Followers</p>
                </div>


                <!-- STATISTIC BLOCK #4 -->
                <div class="col-xs-3 statistic-block text-center m-bottom-50 editContent animated" data-animation="bounceIn" data-animation-delay="800">
                    <span class="ti-time theme-color"></span>
                    <div class="statistic-number">245</div>
                    <p>Sleeping Hours</p>
                </div>


            </div>	 <!-- End row -->
        </div>	 <!-- End container -->
    </div>	 <!-- END STATISTIC-1-2 -->



    <!-- PORTFOLIO-1-2
    ============================================= -->
    <section id="portfolio-1-2" class="wide-80 portfolio-section division bg-edit">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title editContent">
                    <h3>Recent Projects</h3>
                    <p>Aliquam a augue suscipit, bibendum luctus neque vestibulum laoreet rhoncus ipsum, bibendum
                        tempor vulputate varius
                    </p>
                </div>
            </div>


            <!-- PORTFOLIO FILTER BUTTONS -->
            <div class="row">
                <div class="col-xs-12 gallery-filter text-center m-bottom-30 editContent">
                    <div class="btn-toolbar">
                        <div class="btn-group">
                            <span class="filter btn" data-filter="all">All</span>
                            <span class="filter btn" data-filter="illustration">Illustration</span>
                            <span class="filter btn" data-filter="digital_art">Digital Art</span>
                            <span class="filter btn" data-filter="graphic_design">Graphic Design</span>
                            <span class="filter btn" data-filter="web">Web</span>
                        </div>
                    </div>
                </div>
            </div>	<!-- END PORTFOLIO FILTER BUTTONS -->


            <!-- PORTFOLIO IMAGES HOLDER -->
            <div class="row portfolio-items-list">
                <ul>


                    <!-- IMAGE #1 -->
                    <li class="col-sm-6 col-md-4 portfolio-item m-bottom-20 illustration web editContent">
                        <div class="hover-overlay">

                            <!-- Image Link -->
                            <img class="img-responsive" src="{{asset('assets/front_end')}}/images/portfolio/img-1.jpg" alt="Portfolio Image">

                            <!-- Image Zoom -->
                            <a class="image-link" href="{{asset('assets/front_end')}}/images/portfolio/img-1.jpg" title="Portfolio Image">

                                <!-- Overlay Content -->
                                <div class="item-overlay white-color">
                                    <div class="overlay-content">
                                        <h4>Enter Title Here</h4>
                                        <p>Aliquam a augue suscipit, luctus neque laoreet rhoncus tellus congue</p>
                                    </div>
                                </div>

                            </a>   <!-- End Image Zoom -->

                        </div>
                    </li>


                    <!-- IMAGE #2 -->
                    <li class="col-sm-6 col-md-4 portfolio-item m-bottom-20 digital_art graphic_design editContent">
                        <div class="hover-overlay">

                            <!-- Image Link -->
                            <img class="img-responsive" src="{{asset('assets/front_end')}}/images/portfolio/img-2.jpg" alt="Portfolio Image">

                            <!-- Image Zoom -->
                            <a class="image-link" href="{{asset('assets/front_end')}}/images/portfolio/img-2.jpg" title="Portfolio Image">

                                <!-- Overlay Content -->
                                <div class="item-overlay white-color">
                                    <div class="overlay-content">
                                        <h4>Enter Title Here</h4>
                                        <p>Aliquam a augue suscipit, luctus neque laoreet rhoncus tellus congue</p>
                                    </div>
                                </div>

                            </a>   <!-- End Image Zoom -->

                        </div>
                    </li>


                    <!-- IMAGE #3 -->
                    <li class="col-sm-6 col-md-4 portfolio-item m-bottom-20 illustration graphic_design editContent">
                        <div class="hover-overlay">

                            <!-- Image Link -->
                            <img class="img-responsive" src="{{asset('assets/front_end')}}/images/portfolio/img-3.jpg" alt="Portfolio Image">

                            <!-- Image Zoom -->
                            <a class="image-link" href="{{asset('assets/front_end')}}/images/portfolio/img-3.jpg" title="Portfolio Image">

                                <!-- Overlay Content -->
                                <div class="item-overlay white-color">
                                    <div class="overlay-content">
                                        <h4>Enter Title Here</h4>
                                        <p>Aliquam a augue suscipit, luctus neque laoreet rhoncus tellus congue</p>
                                    </div>
                                </div>

                            </a>   <!-- End Image Zoom -->

                        </div>
                    </li>



                    <!-- IMAGE #4 -->
                    <li class="col-sm-6 col-md-4 portfolio-item m-bottom-20 web editContent">
                        <div class="hover-overlay">

                            <!-- Image Link -->
                            <img class="img-responsive" src="{{asset('assets/front_end')}}/images/portfolio/img-4.jpg" alt="Portfolio Image">

                            <!-- Image Zoom -->
                            <a class="image-link" href="{{asset('assets/front_end')}}/images/portfolio/img-4.jpg" title="Portfolio Image">

                                <!-- Overlay Content -->
                                <div class="item-overlay white-color">
                                    <div class="overlay-content">
                                        <h4>Enter Title Here</h4>
                                        <p>Aliquam a augue suscipit, luctus neque laoreet rhoncus tellus congue</p>
                                    </div>
                                </div>

                            </a>   <!-- End Image Zoom -->

                        </div>
                    </li>


                    <!-- IMAGE #5 -->
                    <li class="col-sm-6 col-md-4 portfolio-item m-bottom-20 digital_art graphic_design editContent">
                        <div class="hover-overlay">

                            <!-- Image Link -->
                            <img class="img-responsive" src="{{asset('assets/front_end')}}/images/portfolio/img-5.jpg" alt="Portfolio Image">

                            <!-- Image Zoom -->
                            <a class="image-link" href="{{asset('assets/front_end')}}/images/portfolio/img-5.jpg" title="Portfolio Image">

                                <!-- Overlay Content -->
                                <div class="item-overlay white-color">
                                    <div class="overlay-content">
                                        <h4>Enter Title Here</h4>
                                        <p>Aliquam a augue suscipit, luctus neque laoreet rhoncus tellus congue</p>
                                    </div>
                                </div>

                            </a>   <!-- End Image Zoom -->

                        </div>
                    </li>


                    <!-- IMAGE #6 -->
                    <li class="col-sm-6 col-md-4 portfolio-item m-bottom-20 digital_art illustration web editContent">
                        <div class="hover-overlay">

                            <!-- Image Link -->
                            <img class="img-responsive" src="{{asset('assets/front_end')}}/images/portfolio/img-6.jpg" alt="Portfolio Image">

                            <!-- Image Zoom -->
                            <a class="image-link" href="{{asset('assets/front_end')}}/images/portfolio/img-6.jpg" title="Portfolio Image">

                                <!-- Overlay Content -->
                                <div class="item-overlay white-color">
                                    <div class="overlay-content">
                                        <h4>Enter Title Here</h4>
                                        <p>Aliquam a augue suscipit, luctus neque laoreet rhoncus tellus congue</p>
                                    </div>
                                </div>

                            </a>   <!-- End Image Zoom -->

                        </div>
                    </li>


                </ul>
            </div>		<!-- END PORTFOLIO IMAGES HOLDER -->


        </div>		<!-- End container -->
    </section>	 <!-- END PORTFOLIO-1-2 -->



    <!-- CONTENT-1-8
    ============================================= -->
    <section id="content-1-8" class="bg-lightgrey wide-50 content-section division bg-edit">
        <div class="container">
            <div class="row">


                <!-- CONTENT IMAGE -->
                <div class="col-sm-6 content-1-img text-center m-bottom-50 editContent">
                    <img class="img-responsive" src="{{asset('assets/front_end')}}/images/content-img-8.png" alt="content-image">
                </div>


                <!-- CONTENT TEXT -->
                <div class="col-sm-6 content-1-txt m-bottom-50 p-left-45">

                    <!-- Title -->
                    <h3 class="h3-lg editContent">We have everything to make your business a success</h3>

                    <!-- Text -->
                    <p class="p-lg editContent">Feugiat eros, ac tincidunt ligula massa in est. Ante ipsum primis in luctus
                        cubilia integer congue purus pretium ligula
                    </p>

                    <!-- CONTENT BOX #1 -->
                    <div class="cbox-1 m-top-30 m-bottom-10 editContent">

                        <!-- Icon -->
                        <div class="cbox-1-icon theme-color"><span class="ti-cloud-up"></span></div>

                        <!-- Text -->
                        <div class="cbox-1-txt">
                            <h4>Cloud Backup</h4>
                            <p class="dark-color">Duis efficitur imperdiet pulvinar phasele imperdiet pulvinare etiam nibh, venenatis ac massa
                                ipsum primis dolores ipsum purus
                            </p>
                        </div>

                    </div>

                    <!-- CONTENT BOX #2 -->
                    <div class="cbox-1 m-bottom-10 editContent">

                        <!-- Icon -->
                        <div class="cbox-1-icon theme-color"><span class="ti-time"></span></div>

                        <!-- Text -->
                        <div class="cbox-1-txt">
                            <h4>Quick Access</h4>
                            <p class="dark-color">Duis efficitur imperdiet pulvinar phasele imperdiet pulvinare etiam nibh, venenatis ac massa
                                ipsum primis dolores ipsum purus
                            </p>
                        </div>

                    </div>

                    <!-- CONTENT BOX #3 -->
                    <div class="cbox-1 editContent">

                        <!-- Icon -->
                        <div class="cbox-1-icon theme-color"><span class="ti-shortcode"></span></div>

                        <!-- Text -->
                        <div class="cbox-1-txt">
                            <h4>Organized Code</h4>
                            <p class="dark-color">Duis efficitur imperdiet pulvinar phasele imperdiet pulvinare etiam nibh, venenatis ac massa
                                ipsum primis dolores ipsum purus
                            </p>
                        </div>

                    </div>

                </div>


            </div>	   <!-- End row -->
        </div>	   <!-- End container -->
    </section>	<!-- END CONTENT-1-8 -->

    <!-- PRICING-3-1
                ============================================= -->
    <section id="pricing-3-1" class="bg-scroll wide-50 pricing-section division">
        <div class="container">


            <!-- SECTION TITLE -->
            <div class="row">
                <div class="col-md-10 col-lg-8 col-md-offset-1 col-lg-offset-2 section-title editContent">
                    <h3>Our Pricing</h3>
                    <p>Aliquam a augue suscipit, bibendum luctus neque vestibulum laoreet rhoncus ipsum, bibendum
                        tempor vulputate varius
                    </p>
                </div>
            </div>


            <!-- PRICING TABLES HOLDER -->
            <div class="row pricing-row">


                <!-- PRICE PLAN STANDARD -->
                <div class="col-sm-4 m-top-15 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="300">
                    <div class="pricing-table bg-white text-center">

                        <!-- Table Header  -->
                        <h5 class="h5-lg">Standard</h5>

                        <!-- Plan Price  -->
                        <div class="price">
                            <sup>$</sup>
                            <span>39</span>
                            <p class="theme-color">Per Month</p>
                        </div>

                        <!-- Plan Features  -->
                        <ul class="features">
                            <li>Up to10 Projects</li>
                            <li>10 GB of Storage</li>
                            <li>Up to 250 Users</li>
                            <li>10 mySQL Database</li>
                        </ul>

                        <!-- Table Button  -->
                        <a href="#" class="btn dark-hover">Select Plan</a>

                    </div>
                </div>


                <!-- PRICE PLAN ADVANCED -->
                <div class="col-sm-4 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="600">
                    <div class="pricing-table bg-white text-center highlight">

                        <!-- Table Header  -->
                        <h5 class="h5-lg">Advanced</h5>

                        <!-- Plan Price  -->
                        <div class="price">
                            <sup>$</sup>
                            <span>69</span>
                            <p class="theme-color">Per Month</p>
                        </div>

                        <!-- Plan Features  -->
                        <ul class="features">
                            <li>Unlimited Projects</li>
                            <li>50 GB of Storage</li>
                            <li>Up to 1000 Users</li>
                            <li>10 mySQL Database</li>
                        </ul>

                        <!-- Table Button  -->
                        <a href="#" class="btn dark-hover">Select Plan</a>

                    </div>
                </div>


                <!-- PRICE PLAN ULTIMATE -->
                <div class="col-sm-4 m-top-15 m-bottom-50 editContent animated" data-animation="fadeInUp" data-animation-delay="900">
                    <div class="pricing-table bg-white text-center">

                        <!-- Table Header  -->
                        <h5 class="h5-lg">Ultimate</h5>

                        <!-- Plan Price  -->
                        <div class="price">
                            <sup>$</sup>
                            <span>99</span>
                            <p class="theme-color">Per Month</p>
                        </div>

                        <!-- Plan Features  -->
                        <ul class="features">
                            <li>Unlimited Projects</li>
                            <li>150 GB of Storage</li>
                            <li>Unlimited Users</li>
                            <li>10 mySQL Database</li>
                        </ul>

                        <!-- Table Button  -->
                        <a href="#" class="btn dark-hover">Select Plan</a>

                    </div>
                </div>


            </div>	 <!-- END PRICING TABLES HOLDER -->


            <!-- PRICING NOTICE TEXT  -->
            <div class="row pricing-notice">
                <div class="col-md-12 text-center m-bottom-50 editContent">
                    <p>*To explore <span class="black-color txt-bold">your best plan</span> or for a custom quote, please contact sales at <span class="black-color txt-bold">1-(985)-765-4321</span> </p>
                </div>
            </div>


        </div>    <!-- End container -->
    </section>	<!-- END PRICING-3-1 -->


    <!-- PROMO-1
    ============================================= -->
    <section id="promo-1" class="bg-scroll bg-green p-top-100 promo-section division bg-edit">
        <div class="container white-color">


            <!-- PROMO TEXT -->
            <div class="row">
                <div class="col-md-12 promo-1-txt text-center m-bottom-50 editContent">

                    <!-- Title -->
                    <h2>Endlessly Customizable, Simple & Codeless</h2>

                    <!-- Text -->
                    <p class="p-lg">Maecenas laoreet augue egestas volutpat vulputate suscipit egestas lobortis magna, mauris interdum
                        lectus mauris nec interdum lectus ornare at sagittis
                    </p>

                    <!-- Button -->
                    <a href="#" class="btn btn-medium btn-tra-white dark-hover m-top-15">Get More Information</a>

                </div>
            </div>


            <!-- PROMO IMAGE -->
            <div class="row">
                <div class="col-md-12 promo-1-img text-center">
                    <img class="img-responsive" src="{{asset('assets/front_end')}}/images/macbook-1.png" alt="promo-image">
                </div>
            </div>


        </div>	   <!-- End container -->
    </section>	<!-- END PROMO-1 -->



    <!-- CLIENTS-1-1
    ============================================= -->
    <div id="clients-1-1" class="bg-lightgrey p-top-40 p-bottom-40 clients division bg-edit">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">


                    <!-- CLIENTS CAROUSEL -->
                    <div class="owl-carousel clients-logo-carousel">

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-1.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-2.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-3.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-4.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-5.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-6.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-7.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-3.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-4.png" alt="client_logo">
                        </div>

                        <!-- CLIENT LOGO -->
                        <div class="item">
                            <img class="img-responsive img-customer" src="{{asset('assets/front_end')}}/images/client-1.png" alt="client_logo">
                        </div>

                    </div>	<!-- END CLIENTS CAROUSEL -->


                </div>
            </div>	   <!-- End row -->
        </div>	   <!-- End container -->
    </div>	<!-- END CLIENTS-1-1 -->

    <!-- CONTACTS-1-3
    ============================================= -->
    <section id="contacts-1-3" class="bg-scroll bg-charcoal p-top-80 p-bottom-50 contacts-section division bg-edit">
        <div class="container white-color">
            <div class="row">


                <!-- CONTACT BOX #1 -->
                <div class="col-sm-4 contact-box text-center m-bottom-30 editContent animated" data-animation="fadeInUp" data-animation-delay="300">

                    <!-- Icon -->
                    <span class="ti-mobile theme-color m-bottom-20"></span>

                    <!-- Text -->
                    <h4 class="h4-lg">Let's Talk</h4>
                    <p class="p-lg">Phone: 1-123-456-4567</p>
                    <p class="p-lg">Fax: 1-987-654-3210</p>

                </div>


                <!-- CONTACT BOX #2 -->
                <div class="col-sm-4 contact-box text-center p-left-45 p-right-45 m-bottom-30 editContent animated" data-animation="fadeInUp" data-animation-delay="500">

                    <!-- Icon -->
                    <span class="ti-map-alt theme-color m-bottom-20"></span>

                    <!-- Text -->
                    <h4 class="h4-lg">Our Location</h4>
                    <p class="p-lg">121 King Street, Melbourne Victoria 3000 Australia</p>

                </div>


                <!-- CONTACT BOX #3 -->
                <div class="col-sm-4 contact-box text-center m-bottom-30 editContent animated" data-animation="fadeInUp" data-animation-delay="700">

                    <!-- Icon -->
                    <span class="ti-email theme-color m-bottom-20"></span>

                    <!-- Text -->
                    <h4 class="h4-lg">Drop A Line</h4>
                    <p class="p-lg">General: hello@domain.com</p>
                    <p class="p-lg">Career: career@domain.com</p>

                </div>


            </div>	  <!-- End row -->
        </div>	   <!-- End container -->
    </section>	<!-- END CONTACTS-1-3 -->



    <!-- CONTACTS-3-3
    ============================================= -->
    <section id="contacts-3-3" class="wide-50 contacts-section division bg-edit">
        <div class="container">
            <div class="row">


                <!-- CONTACT FORM -->
                <div class="col-sm-6 contacts-form m-bottom-50 editContent">

                    <!-- Title -->
                    <h3>Contact Us</h3>

                    <!-- Text -->
                    <p>Sed varius leo quis luctus posuere. Mauris sed porta elementum nec vel orci. Praesent id pharetra est. Nunc
                        ligula arcu, in quam non, mattis  donec diam euismod vehicula pharetra ultrice sapien egestas lobortis magna
                    </p>

                    <form name="contactform" class="row m-top-25 contact-form">

                        <!-- Contact Form Input -->
                        <div class="col-md-12 input-name">
                            <input type="text" name="name" class="form-control name" placeholder="Name">
                        </div>

                        <div class="col-md-12 input-email">
                            <input type="text" name="email" class="form-control email" placeholder="Email">
                        </div>

                        <div class="col-md-12 input-subject">
                            <input type="text" name="subject" class="form-control subject" placeholder="Subject">
                        </div>

                        <div class="col-md-12 input-message">
                            <textarea class="form-control message" name="message" rows="6" placeholder="Your Message ..."></textarea>
                        </div>

                        <!-- Contact Form Button -->
                        <div class="col-md-12 form_btn">
                            <input type="submit" value="Send Your Message" class="btn btn-medium dark-hover submit m-top-10">
                        </div>

                        <!-- Contact Form Message -->
                        <div class="col-md-12 contact-form-msg">
                            <span class="theme-color loading"></span>
                        </div>

                    </form>
                </div>


                <!-- GOOGLE MAP -->
                <div class="col-sm-6 contacts-map p-left-30 m-bottom-50 editContent">

                    <!-- Title -->
                    <h3>Find Us On Map</h3>

                    <!-- Text -->
                    <p>Sed varius leo quis luctus posuere. Mauris sed porta elementum nec vel orci. Praesent id pharetra est. Nunc
                        ligula arcu, in quam non, mattis
                    </p>

                    <div class="google-map m-top-25">
                        <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it!	-->
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.835129751791!2d144.95346311531895!3d-37.81733057975167!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sru!2sau!4v1454842310427" width="600" height="450" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>


            </div>		<!-- End row -->
        </div>	   <!-- End container -->
    </section>	<!-- END CONTACTS-3-3 -->



    <!-- CALL TO ACTION-4
    ============================================= -->
    <section id="call-to-action-4" class="bg-scroll bg-theme p-top-50 p-bottom-50 call-to-action division bg-edit">
        <div class="container white-color">
            <div class="row">


                <!-- Title -->
                <div class="col-md-9 col-lg-8 cta-4-txt text-right p-right-45 editContent">
                    <h3>Buy PowerNode now and try everything</h3>
                </div>

                <!-- Button -->
                <div class="col-md-3 col-lg-4 cta-4-btn editContent">
                    <a href="#" class="btn btn-medium btn-tra-white dark-hover">Purchase Now</a>
                </div>


            </div>	   <!-- End row -->
        </div>		<!-- End container -->
    </section>	 <!-- END CALL TO ACTION-4 -->



    <!-- FOOTER-1-4
    ============================================= -->
    <footer id="footer-1-4" class="bg-black footer division bg-edit">


        <!-- PRE-FOOTER -->
        <div class="p-top-80 p-bottom-30 pre-footer bg-edit">
            <div class="container">
                <div class="row">


                    <!-- FOOTER ABOUT WIDGET -->
                    <div class="col-sm-6 col-md-4 footer-about-widget m-bottom-50 editContent">

                        <!-- Title -->
                        <h4 class="h4-lg white-color">About Us</h4>

                        <!-- Text -->
                        <p class="grey-color m-bottom-20">Maecenas laoreet augue egestas volutpat suscipit egestas lobortis magna, sit amet suscipit lobortis
                            magna sodales suscipit egestas quaerat hendrerit sapien undo euismod blandit
                        </p>

                        <p class="grey-color txt-medium m-bottom-5"><span class="ti-location-pin m-right-10"></span>121 King St, Melbourne VIC 3000, Australia</p>
                        <p class="grey-color txt-medium m-bottom-5"><span class="ti-mobile m-right-10"></span>1-123-456-7890</p>
                        <p class="grey-color txt-medium m-bottom-5"><span class="ti-email m-right-10"></span>hello@yourdomain.com</p>

                    </div>


                    <!-- FOOTER NEWS WIDGET -->
                    <div class="col-sm-6 col-md-3 p-right-30 footer-news-widget m-bottom-50 editContent">

                        <!-- Title -->
                        <h4 class="h4-lg white-color">Latest Blog News</h4>

                        <ul class="footer-news clearfix">
                            <li><a class="foo-news" href="#">Cursus sodales vitae suscipit <span>May 20, 2016</span></a></li>
                            <li><a class="foo-news" href="#">Dapibus egestas lobortis magna phasellus nec euismod<span>May 08, 2016</span></a></li>
                            <li><a class="foo-news" href="#">Legestas lobortis magna, sit amet suscipit<span>June 21, 2016</span></a></li>
                        </ul>

                    </div>


                    <!-- FOOTER MENU WIDGET -->
                    <div class="col-sm-6 col-md-2 footer-menu-widget m-bottom-50 editContent">

                        <!-- Title -->
                        <h4 class="h4-lg white-color">More Links</h4>

                        <!-- Links List -->
                        <ul class="footer-1-links clearfix">
                            <li><a class="foo-1-link" href="#">About Our Team</a></li>
                            <li><a class="foo-1-link" href="#">Our Services</a></li>
                            <li><a class="foo-1-link" href="#">FAQs</a></li>
                            <li><a class="foo-1-link" href="#">Our Services</a></li>
                            <li><a class="foo-1-link" href="#">Maintenance</a></li>
                            <li><a class="foo-1-link" href="#">Contact Us</a></li>
                            <li><a class="foo-1-link" href="#">Career</a></li>
                        </ul>

                    </div>


                    <!-- FOOTER TAG WIDGET -->
                    <div class="col-sm-6 col-md-3 footer-tags-widget m-bottom-50 editContent">

                        <!-- Title -->
                        <h4 class="h4-lg white-color">Tags</h4>

                        <!-- Tags List -->
                        <ul class="footer-tags clearfix">
                            <li><a class="foo-tag" href="#">design</a></li>
                            <li><a class="foo-tag" href="#">fashion</a></li>
                            <li><a class="foo-tag" href="#">minimal</a></li>
                            <li><a class="foo-tag" href="#">workspace</a></li>
                            <li><a class="foo-tag" href="#">style</a></li>
                            <li><a class="foo-tag" href="#">simple</a></li>
                            <li><a class="foo-tag" href="#">jquery</a></li>
                            <li><a class="foo-tag" href="#">javascript</a></li>
                            <li><a class="foo-tag" href="#">stylesheet</a></li>
                            <li><a class="foo-tag" href="#">css</a></li>
                            <li><a class="foo-tag" href="#">travel</a></li>
                            <li><a class="foo-tag" href="#">enjoy</a></li>
                            <li><a class="foo-tag" href="#">holiday</a></li>
                        </ul>

                    </div>


                </div>	  <!-- Eтd row -->
            </div>	  <!-- End container -->
        </div>	  <!-- END PRE-FOOTER -->


        <!-- FOOTER COPYRIGHT -->
        <div class="container footer-bottom p-top-20 p-bottom-20">
            <div class="row">
                <div class="col-md-12 footer-copyright editContent">
                    <p class="m-bottom-0 grey-color">&copy; <?php echo date("Y") ?> Eventati. All Rights Reserved</p>
                </div>
            </div>	<!-- End row -->
        </div>	<!-- End container -->


    </footer>	<!-- END FOOTER-1-4 -->



</div>	<!-- END PAGE CONTENT -->
<!-- EXTERNAL SCRIPTS
============================================= -->
<script src="{{asset('assets/front_end')}}/js/jquery-2.2.4.min.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/modernizr.custom.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.easing.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/retina.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.stellar.min.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.scrollto.js" type="text/javascript"></script>
<script defer src="{{asset('assets/front_end')}}/js/typed.js" type="text/javascript"></script>
<script defer src="{{asset('assets/front_end')}}/js/jquery.appear.js" type="text/javascript"></script>
<script defer src="{{asset('assets/front_end')}}/js/jquery.vide.min.js" type="text/javascript"></script>
<script defer src="{{asset('assets/front_end')}}/js/jquery.countdown.min.js" type="text/javascript"></script>
<script type="text/javascript" src="{{asset('assets/front_end')}}/js/goalProgress.js"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.easypiechart.min.js"></script>
<script src="{{asset('assets/front_end')}}/js/owl.carousel.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.mixitup.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/progress-bar.js" type="text/javascript"></script>
<script defer src="{{asset('assets/front_end')}}/js/jquery.flexslider.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.ajaxchimp.min.js"></script>
<script src="{{asset('assets/front_end')}}/js/contact_form.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/callback_form.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/register_form.js" type="text/javascript"></script>
<script src="{{asset('assets/front_end')}}/js/jquery.validate.min.js" type="text/javascript"></script>

<!-- Custom Script -->
<script src="{{asset('assets/front_end')}}/js/custom.js" type="text/javascript"></script>


<!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
<!--[if lt IE 9]>
<script src="{{asset('assets/front_end')}}/js/html5shiv.js"></script>
<script src="{{asset('assets/front_end')}}/js/respond.min.js"></script>
<![endif]-->

<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->


<!--
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script>
-->
</body>
</html>