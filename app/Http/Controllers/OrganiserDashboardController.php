<?php

namespace App\Http\Controllers;

use App\Models\Organiser;
use App\Models\EventStats;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use DateTime;
use Illuminate\Support\Facades\Auth;

class OrganiserDashboardController extends MyBaseController
{
    /**
     * Show the organiser dashboard
     *
     * @param $organiser_id
     * @return mixed
     */
    public function showDashboard($organiser_id)
    {
        $account_type = Auth::user()->account_type;

        $num_days = 20;
        $result = [];
        $tickets_data = [];

        $organiser = Organiser::scope()->findOrFail($organiser_id);
        $upcoming_events = $organiser->events()->where('end_date', '>=', Carbon::now())->get();
        $calendar_events = [];

        if($account_type === 1){
            $organizers = Organiser::all();
            foreach ($organizers as $person){
                /* Prepare JSON array for events for use in the dashboard calendar */
                foreach ($person->events as $event) {
                    $calendar_events[] = [
                        'title' => $event->title,
                        'start' => $event->start_date->toIso8601String(),
                        'end'   => $event->end_date->toIso8601String(),
                        'url'   => route('showEventDashboard', [
                            'event_id' => $event->id
                        ]),
                        'color' => '#4E558F'
                    ];

                    $chartData = EventStats::where('event_id', '=', $event->id)
                        ->where('date', '>', Carbon::now()->subDays($num_days)->format('Y-m-d'))
                        ->get()
                        ->toArray();

                    $startDate = new DateTime("-$num_days days");
                    $dateItter = new DatePeriod(
                        $startDate, new DateInterval('P1D'), $num_days
                    );

                    foreach ($dateItter as $date) {
                        $views = 0;
                        $sales_volume = 0;
                        $unique_views = 0;
                        $tickets_sold = 0;
                        $organiser_fees_volume = 0;

                        foreach ($chartData as $item) {
                            if ($item['date'] == $date->format('Y-m-d')) {
                                $views = $item['views'];
                                $sales_volume = $item['sales_volume'];
                                $organiser_fees_volume = $item['organiser_fees_volume'];
                                $unique_views = $item['unique_views'];
                                $tickets_sold = $item['tickets_sold'];

                                break;
                            }
                        }

                        $result[] = [
                            'date'         => $date->format('Y-m-d'),
                            'views'        => $views,
                            'unique_views' => $unique_views,
                            'sales_volume' => $sales_volume + $organiser_fees_volume,
                            'tickets_sold' => $tickets_sold,
                        ];
                    }

                    foreach ($event->tickets as $ticket) {
                        $tickets_data[] = [
                            'value' => $ticket->quantity_sold,
                            'label' => $ticket->title,
                        ];
                    }

                }
            }
        }else {
            /* Prepare JSON array for events for use in the dashboard calendar */
            foreach ($organiser->events as $event) {
                $calendar_events[] = [
                    'title' => $event->title,
                    'start' => $event->start_date->toIso8601String(),
                    'end'   => $event->end_date->toIso8601String(),
                    'url'   => route('showEventDashboard', [
                        'event_id' => $event->id
                    ]),
                    'color' => '#4E558F'
                ];
            }
        }
        $data = [
            'organiser'       => $organiser,
            'upcoming_events' => $upcoming_events,
            'calendar_events' => json_encode($calendar_events),
            'chartData'  => json_encode($result),
            'ticketData' => json_encode($tickets_data),
        ];


        if($account_type === 1){
            return view('ManageSuperuser.Dashboard', $data);
        }

        return view('ManageOrganiser.Dashboard', $data);
    }
}
